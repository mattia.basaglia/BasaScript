/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \section License
 *
 * Copyright (C) 2015 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#ifndef OBJECT_STRING_HPP
#define OBJECT_STRING_HPP

#include "object.hpp"

#include <string>

namespace object {

/**
 * \brief Wraps a string
 */
class String : public Object
{
public:
    String(std::string string={}) : string(std::move(string)) {}

    /**
     * \brief Returns the contained std::string
     */
    const std::string& str() const { return string; }

protected:
    util::Any convert(const std::type_info& type) const override
    {
        if ( type == typeid(std::string) )
            return string;
        return {};
    }

    bool to_stream(std::ostream& os) const override
    {
        return os << string;
    }

    bool from_stream(std::istream& os) override
    {
        return os >> string;
    }

private:
    std::string string;
};

} // namespace object
#endif // OBJECT_STRING_HPP
