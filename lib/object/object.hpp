/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \section License
 *
 * Copyright (C) 2015 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#ifndef OBJECT_OBJECT_HPP
#define OBJECT_OBJECT_HPP

#include <memory>
#include <string>
#include <vector>
#include <typeinfo>

#include <boost/type_traits.hpp>

#include "util/flags.hpp"
#include "util/any.hpp"

namespace object {

class Object;

/**
 * \brief Contains an \c Object
 */
using Pointer = std::shared_ptr<Object>;

/**
 * \brief Creates a \c Pointer
 */
template<class T, class... Args>
auto New (Args&&... args)
{
    return std::make_shared<T>(std::forward<Args>(args)...);
}

/**
 * \brief Used to refer to object members
 */
using Name = std::string;

/**
 * \brief Arguments passed to a function
 */
using Arguments = std::vector<Pointer>;

/**
 * \brief Enumerator listing possible operations on object members
 */
enum MemberProperty
{
    Get         = 0x01, ///< Can be read as a value
    Set         = 0x02, ///< Can be used to store values
    Call        = 0x04, ///< Can be called like a function
};

using MemberProperties = util::Flags<MemberProperty>;

/**
 * \brief Base of the hierarchy for run-time types
 */
class Object
{
public:
    virtual ~Object() {}
    
    /**
     * \brief Get a property by name
     */
    virtual Pointer get(const Name& name) const;
    /**
     * \brief Set a property by name
     */
    virtual Pointer set(const Name& name, const Pointer& value);
    /**
     * \brief Call a method by name
     */
    virtual Pointer call(const Name& name, const Arguments& args);
    /**
     * \brief Check if a member is available for the given set of operations
     */
    virtual bool enquire(const Name& name, MemberProperties properties) const;

    /**
     * \brief Converts to the given type
     * \throws std::logic_error if the conversion fails
     */
    template<class T>  auto to() const { return to_impl<T>(); }

    /**
     * \brief Converts to the given type
     * \returns The appropriate value or \c default_value if the conversion fails
     */
    template<class T>  auto to(const T& default_value) const
    try { return to_impl<T>(); }
    catch(const std::logic_error&) { return default_value; }

protected:
    /**
     * \brief Converts to the type corresponding to the given type info
     * \returns an util::Any which is either empty or with the same type as the one passed
     */
    virtual util::Any convert(const std::type_info& type) const;

    /**
     * \brief Writes the object to a stream
     * \returns \b true on success
     */
    virtual bool to_stream(std::ostream& os) const;
    /**
     * \brief Reads the object from a stream
     * \returns \b true on success
     */
    virtual bool from_stream(std::istream& os);

private:
    /**
     * \brief Utility function used by other templates
     * \tparam T \b RealType in to_impl()
     */
    template<class T>
    static constexpr bool can_stream() {
        return std::is_default_constructible<T>::value &&
            std::is_move_constructible<T>::value &&
            boost::has_left_shift<std::ostream&,T>::value;
    }

    /**
     * \brief Implements to()
     * First it attempts to call convert(), which might have been specialized.
     * If that fails it tries cnverting through a stream.
     * If that fails as well, it throws an exception
     * \tparam T Type to convert to
     */
    template<class T>
        std::decay_t<T> to_impl() const
        {
            using RealType = std::decay_t<T>;
            const auto& type_info = typeid(RealType);
            auto any = convert(type_info);
            if ( any.empty() )
            {
                return failed_conversion<RealType>();
            }
            if ( any.type() != type_info )
                throw std::logic_error("Faulty conversion requested");
            return util::any_cast<RealType>(any);
        }

    /**
     * \brief Base case for failed conversion
     * \throws std::logic_error
     */
    template<class T, class = std::enable_if_t<!can_stream<T>()>>
        T failed_conversion() const
        {
            throw std::logic_error("Invalid conversion requested");
        }

    /**
     * \brief Stream conversion
     * \throws std::logic_error if the conversion fails
     * \note SFINAE because can't partially specialize functions
     */
    template<class T, class = std::enable_if_t<can_stream<T>()>, class=void>
        T failed_conversion() const
        {
            std::stringstream ss;
            if ( to_stream(ss) )
            {
                T temp;
                if ( ss >> temp )
                    return std::move(temp);
            }
            throw std::logic_error("Invalid conversion requested");
        }
};

} // namespace object
#endif // OBJECT_OBJECT_HPP
