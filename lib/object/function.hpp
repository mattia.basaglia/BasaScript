/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \section License
 *
 * Copyright (C) 2015 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#ifndef OBJECT_FUNCTION_HPP
#define OBJECT_FUNCTION_HPP

#include "object.hpp"

namespace object {

namespace detail {

/**
 * \brief Dummy class to a get compile-time sequence of indices from a parameter pack
 * \tparam Indices Sequence if indices from 0 to sizeof...(Indices)
 */
template <int... Indices>
    struct IndexPack {};

/**
 * \brief Dummy class that builds an integer pack from a single integer
 * \param N The number which needs to be converted to a pack
 * \param Indices Pack of indices for \c IndexPack, starts out empty
 */
template <int N, int... Indices>
    struct IndexPackBuilder : IndexPackBuilder<N-1, N-1, Indices...> {};
/**
 * \brief Termination for \c IndexPackBuilder
 */
template <int... Indices>
    struct IndexPackBuilder<0, Indices...> : IndexPack<Indices...> {};

/**
 * \brief Helper for \c call(), uses the \c IndexPack to extract the arguments
 * \tparam Ret          Return type
 * \tparam Args         Function parameter types
 * \tparam Indices      Pack of indices for \c Args, deduced by the dummy parameter
 */
template<class Ret, class... Args, int... Indices>
    Ret call_helper(Ret(*func)(Args...), const Arguments& args, IndexPack<Indices...>)
    {
        if ( args.size() != sizeof...(Args) )
            throw std::invalid_argument("Wrong number of arguments");
        return func(args[Indices]->to<Args>()...);
    }

/**
 * \brief Dummy function to find the return type of a pointer to member function
 * (Not needed in C++14)
 */
template <class Class, class Ret, class... Args>
Ret member_return(Ret(Class::*)(Args...) const);
template <class Class, class Ret, class... Args>
Ret member_return(Ret(Class::*)(Args...));

/**
 * \brief Dummy function to build an index pack for the parameters of a pointer to member function
 */
template <class Class, class Ret, class... Args, class Index = IndexPackBuilder<sizeof...(Args)>>
Index member_index(Ret(Class::*)(Args...) const){ return {}; }
template <class Class, class Ret, class... Args, class Index = IndexPackBuilder<sizeof...(Args)>>
Index member_index(Ret(Class::*)(Args...)){ return {}; }

/**
 * \brief Helper for \c call(), uses the \c IndexPack to extract the arguments
 * \tparam Ret          Return type
 * \tparam Args         Function parameter types
 * \tparam Indices      Pack of indices for \c Args, deduced by the dummy parameter
 */
template<class Functor, class Ret, class... Args, int... Indices>
    Ret call_functor_helper(const Functor& functor, const Arguments& args,
                            Ret(Functor::*)(Args...) const, IndexPack<Indices...>)
    {
        if ( args.size() != sizeof...(Args) )
            throw std::invalid_argument("Wrong number of arguments");
        return functor(args[Indices]->to<Args>()...);
    }
template<class Functor, class Ret, class... Args, int... Indices>
    Ret call_functor_helper(Functor& functor, const Arguments& args,
                            Ret(Functor::*)(Args...), IndexPack<Indices...>)
    {
        if ( args.size() != sizeof...(Args) )
            throw std::invalid_argument("Wrong number of arguments");
        return functor(args[Indices]->to<Args>()...);
    }

} // namespace detail

/**
 * \brief Calls a function pointer with the given vector as arguments
 */
template<class Ret, class... Args>
    Ret call(Ret(*func)(Args...), const Arguments& args)
{
    return detail::call_helper(func, args, detail::IndexPackBuilder<sizeof...(Args)>{});
}

/**
 * \brief Calls a functor with the given vector as arguments
 */
template<class Functor, class = typename std::enable_if<std::is_member_function_pointer<decltype(&Functor::operator())>::value>::type>
    decltype(detail::member_return(&Functor::operator()))
        call(const Functor& func, const Arguments& args)
{
    return detail::call_functor_helper(func, args, &Functor::operator(), detail::member_index(&Functor::operator()));
}
template<class Functor, class = typename std::enable_if<std::is_member_function_pointer<decltype(&Functor::operator())>::value>::type>
    decltype(detail::member_return(&Functor::operator()))
        call(Functor& func, const Arguments& args)
{
    return detail::call_functor_helper(func, args, &Functor::operator(), detail::member_index(&Functor::operator()));
}

} // namespace object
#endif // OBJECT_FUNCTION_HPP
