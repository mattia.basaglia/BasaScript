/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \section License
 *
 * Copyright (C) 2015 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#ifndef UTIL_FLAGS_HPP
#define UTIL_FLAGS_HPP
#include <type_traits>
namespace util {

template<typename Enum>
class Flags
{
public:
    using int_type = typename std::underlying_type<Enum>::type;
    using enum_type = Enum;

    constexpr Flags(Enum e) : val(int_type(e)) {}
    constexpr Flags() : val(0) {}
    constexpr Flags(int_type val) : val(val) {}

    constexpr Flags operator| (Flags rhs) const { return Flags(val|rhs.val); }
    friend constexpr Flags operator| (Enum lhs, Flags rhs) { return rhs|lhs; }
    friend constexpr Flags operator| (int_type lhs, Flags rhs) { return rhs|lhs; }
    constexpr Flags& operator|= (Flags rhs) { val |= rhs.val; return *this; }

    constexpr Flags operator& (Flags rhs) const { return Flags(val&rhs.val); }
    constexpr Flags operator& (int_type rhs) const { return Flags(val&rhs); }
    friend constexpr Flags operator& (Enum lhs, Flags rhs) { return rhs&lhs; }
    friend constexpr Flags operator& (int_type lhs, Flags rhs) { return rhs&lhs; }
    constexpr Flags& operator&= (Flags rhs) { val &= rhs.val; return *this; }

    constexpr Flags operator^ (Flags rhs) const { return Flags(val^rhs.val); }
    friend constexpr Flags operator^ (Enum lhs, Flags rhs) { return rhs^lhs; }
    friend constexpr Flags operator^ (int_type lhs, Flags rhs) { return rhs^lhs; }
    constexpr Flags& operator^= (Flags rhs) { val ^= rhs.val; return *this; }

    constexpr Flags operator~() const { return Flags(~val); }
    friend constexpr Flags operator~(Enum e) { return ~Flags(e); }

    constexpr bool operator== (Flags rhs) const { return val == rhs.val; }
    friend constexpr bool operator== (Enum lhs, Flags rhs) { return rhs == lhs; }
    friend constexpr bool operator== (int_type lhs, Flags rhs) { return rhs == lhs; }
    constexpr bool operator!= (Flags rhs) const { return val != rhs.val; }
    friend constexpr bool operator!= (Enum lhs, Flags rhs) { return rhs != lhs; }
    friend constexpr bool operator!= (int_type lhs, Flags rhs) { return rhs != lhs; }
    constexpr bool operator!() const { return !val; }

    explicit constexpr operator int_type() const { return val; }
    explicit constexpr operator bool() const { return val; }

    constexpr bool has_flag(Flags rhs) { return (val & rhs.val) == rhs.val; }


private:
    int_type val;
};

} // namespace util
#endif // UTIL_FLAGS_HPP
