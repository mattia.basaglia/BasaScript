/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \section License
 *
 * Copyright (C) 2015 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#ifndef UTIL_ANY_HPP
#define UTIL_ANY_HPP

#if __has_include(<optional>)
#   include<any>
    namespace util {
        using Any = std::any;
        using std::any_cast;
    }
#elif __has_include(<experimental/any>)
#   include <experimental/any>
    namespace util {
        using Any = std::experimental::any
        using std::experimental::any_cast;
    }
#elif __has_include(<boost/any.hpp>)
#   include <boost/any.hpp>
    namespace util {
        using Any = boost::any;
        using boost::any_cast;
    }
#else
#   error "Missing <any>"
#endif // __has_include


#endif // UTIL_ANY_HPP
