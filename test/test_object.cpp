/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \section License
 *
 * Copyright (C) 2015 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#define BOOST_TEST_MODULE Test_Object

#include <boost/test/unit_test.hpp>

#include "object/string.hpp"
#include "object/function.hpp"

using namespace object;

BOOST_AUTO_TEST_CASE( test_string )
{
    String s("foobar");
    BOOST_CHECK( s.str() == "foobar" );
    BOOST_CHECK( s.to<std::string>() == "foobar" );
    BOOST_CHECK( s.to<int>(5) == 5 );

    Pointer obj = New<String>("123");
    BOOST_CHECK( obj->to<std::string>() == "123" );
    BOOST_CHECK( obj->to<int>() == 123 );
    BOOST_CHECK( obj->to<int>(5) == 123 );
}

double divtest(int a, double b)
{
    return a / b;
}

BOOST_AUTO_TEST_CASE( test_function )
{
    BOOST_CHECK( call(divtest, {New<String>("5"), New<String>("2")}) == 2.5 );
    BOOST_CHECK( call([](int a, int b){return a+b;}, {New<String>("5"), New<String>("2")}) == 7 );
    auto lambda = [](int a, int b) mutable {return a+b;};
    BOOST_CHECK( call(lambda, {New<String>("5"), New<String>("2")}) == 7 );
}
