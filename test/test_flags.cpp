/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \section License
 *
 * Copyright (C) 2015 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#define BOOST_TEST_MODULE Test_Math

#include "util/flags.hpp"

#include <boost/test/unit_test.hpp>

enum Test : int
{
    TestNo = 0x0,
    TestA  = 0x1,
    TestB  = 0x2,
    TestC  = 0x4
};


BOOST_AUTO_TEST_CASE( test_flags_enum )
{

    using TestFlags = util::Flags<Test>;

    BOOST_CHECK( TestFlags(TestNo) == TestFlags() );
    BOOST_CHECK( TestFlags(TestA) == TestFlags(1) );
    BOOST_CHECK( TestFlags(TestA|TestB) == 3 );

    BOOST_CHECK( int(TestFlags(TestA)|TestB) == 3 );
    BOOST_CHECK( int(TestB|TestFlags(TestA)) == 3 );
    BOOST_CHECK( int(6|TestFlags(TestA)) == 7 );

    BOOST_CHECK( int(TestFlags(TestA|TestB|TestC)&3) == 3 );
    BOOST_CHECK( int(TestFlags(TestA|TestB|TestC)&TestFlags(TestA|TestB)) == 3 );
    BOOST_CHECK( int(TestFlags(TestA|TestB|TestC)&TestA) == 1 );
    BOOST_CHECK( int(TestA&TestFlags(TestA|TestB|TestC)) == 1 );
    BOOST_CHECK( int(TestFlags(TestA|TestB|TestC)&3) == 3 );
    BOOST_CHECK( int(3&TestFlags(TestA|TestB|TestC)) == 3 );

    BOOST_CHECK( int(TestFlags(TestA|TestB)^TestFlags(TestB|TestC)) == 5 );
    BOOST_CHECK( int(TestFlags(TestA)^TestC) == 5 );
    BOOST_CHECK( int(TestFlags(TestA|TestB)^6) == 5 );
    BOOST_CHECK( int(TestC^TestFlags(TestA)) == 5 );
    BOOST_CHECK( int(6^TestFlags(TestA|TestB)) == 5 );

    BOOST_CHECK( ~TestFlags(TestA) == ~TestA );

}

enum class Class : int
{
    No = 0x0,
    A  = 0x1,
    B  = 0x2,
    C  = 0x4
};

BOOST_AUTO_TEST_CASE( test_flags_enum_class )
{
    using TestFlags = util::Flags<Class>;

    BOOST_CHECK( TestFlags(Class::No) == TestFlags() );
    BOOST_CHECK( TestFlags(Class::A) == TestFlags(1) );

    BOOST_CHECK( int(TestFlags(Class::A)|Class::B) == 3 );
    BOOST_CHECK( int(Class::B|TestFlags(Class::A)) == 3 );
    BOOST_CHECK( int(6|TestFlags(Class::A)) == 7 );

    BOOST_CHECK( int(TestFlags(7)&3) == 3 );
    BOOST_CHECK( int(TestFlags(7)&TestFlags(3)) == 3 );
    BOOST_CHECK( int(TestFlags(7)&Class::A) == 1 );
    BOOST_CHECK( int(Class::A&TestFlags(7)) == 1 );
    BOOST_CHECK( int(TestFlags(7)&3) == 3 );
    BOOST_CHECK( int(3&TestFlags(7)) == 3 );

    BOOST_CHECK( int(TestFlags(3)^TestFlags(6)) == 5 );
    BOOST_CHECK( int(TestFlags(Class::A)^Class::C) == 5 );
    BOOST_CHECK( int(TestFlags(3)^6) == 5 );
    BOOST_CHECK( int(Class::C^TestFlags(Class::A)) == 5 );
    BOOST_CHECK( int(6^TestFlags(3)) == 5 );

    BOOST_CHECK( ~TestFlags(Class::A) == ~int(Class::A) );

}
